package integration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

public class IntegrationBase {
	  public static final Integer DEFAULT_WAITFOR_PAGE_SECONDS;
	    public static final Integer DEFAULT_WAITFOR_AJAX_SECONDS;
	    
	    static {
	        //   BASIC_URL = "https://app.wordstream.com/free-trial?is_big=t&is_orange=t";
	           DEFAULT_WAITFOR_PAGE_SECONDS = 30;
	           DEFAULT_WAITFOR_AJAX_SECONDS = 30;
	       }
	    
	    protected WebDriver driver;
	    protected String browser;


	    public IntegrationBase(String browser) {
	        this.browser = browser;
	    }
	    @BeforeTest
	    public final void setup() {
	        driver = createWebDriver();
	    }

	    private WebDriver createWebDriver() {
	        System.setProperty("webdriver.chrome.driver", "D:\\java\\chromedriver.exe");
	        driver = new ChromeDriver();
	              return driver;
	    }


	    public WebDriver getDriver() {
	        return driver;
	    }

	    public void setDriver(WebDriver driver) {
	        this.driver = driver;
	    }

	    /*  @AfterMethod
	       public final void tearDown() {
	           if (driver != null) {
	               driver.quit();
	           }
	       } */
	    
}
