package pagemodel;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import integration.AbstractWebComponent;


public class SearchPage extends AbstractWebComponent{

	public SearchPage(WebDriver driver) {
		super(driver);
		
	}
	
	//locators
    @FindBy(css = "body")
    public WebElement bodySearchPage;
	
    @FindBy(id = "lst-ib")
    public WebElement googleSearchField;
    
    @FindBy(css = "#yschsp")
    public WebElement yahooSearchField;
    
    @FindBy(id = "sb_form_q")
    public WebElement bingSearchField;
    
    @FindBy(xpath = " //div/h3/a | //h2/a")
    public List <WebElement> resultLinks;
	
	
	
	//variables
	   private static final String GOOGLE_COM = "https://www.google.com";
	    private static final String YAHOO_COM = "https://search.yahoo.com/";
	    private static final String BING_COM = "https://bing.com";
	    
	    private static final String SAUCELAB = "saucelabs";
	    private static final String GITHUB= "github";
	    private static final String BITBUCKET= "bitbucket";
	
	//methods
	    public void waitForPageToLoad() {
	        //waitForPage.until(ExpectedConditions.visibilityOf(bodySearchPage));
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    }
	    
	   public void openGooglepage(String word) {
		   driver.get(GOOGLE_COM);
		   googleSearchField.clear();
		   googleSearchField.sendKeys(word);
		   googleSearchField.sendKeys(Keys.RETURN);
		
		   
	    }
	   
	   public void openYahoopage(String word) {
		   driver.get(YAHOO_COM);
		   yahooSearchField.clear();
		   yahooSearchField.sendKeys(word);
		   yahooSearchField.sendKeys(Keys.RETURN);
		
		   
	    }
	   
	   public void openBingpage(String word) {
		   driver.get(BING_COM);
		   bingSearchField.clear();
		   bingSearchField.sendKeys(word);
		   bingSearchField.sendKeys(Keys.RETURN);
		   
	    }
	   
	   public int countWords(){
		   
		   
		   int resultCount= resultLinks.size();
		   //System.out.println(resultCount);
		   return resultCount;
		 
	        //System.out.println("Searchable word \"" + word + "\" for search engine " + searchEngine + " first link is - " + count + " times");
	   }
	   
	   public void compareResultNumber(String word){
		  openGooglepage(word);
		 waitForPageToLoad();
		   int googleResult = countWords();
		   openYahoopage(word);
		   waitForPageToLoad();
		   int yahooResult = countWords();
		   openBingpage(word);
		   waitForPageToLoad();
		   int bingResult = countWords();
		   
		   if(googleResult>yahooResult && googleResult>bingResult){
			   System.out.println("Largest search results value is " + googleResult +" showing by " + GOOGLE_COM);
			   if (yahooResult>bingResult)
			   {System.out.println("Medium search results value is " + yahooResult +" showing by " + YAHOO_COM);
			    System.out.println("Smallest search results value is " + bingResult +" showing by " + BING_COM); }
			   else{
				   System.out.println("Medium search results value is " + bingResult +" showing by " +  BING_COM);
				    System.out.println("Smallest search results value is " + yahooResult +" showing by " +  YAHOO_COM);
				   
			   }
		   }
		   
		   else if(yahooResult>googleResult && yahooResult>bingResult){
			   System.out.println("Largest search results value is " + yahooResult +" showing by " + YAHOO_COM);
			
			   if (googleResult>bingResult)
			   {System.out.println("Medium search results value is " + googleResult +" showing by " + GOOGLE_COM);
			    System.out.println("Smallest search results value is " + bingResult +" showing by " + BING_COM); }
			   else{
				   System.out.println("Medium search results value is " + bingResult +" showing by " +  BING_COM);
				    System.out.println("Smallest search results value is " + googleResult +" showing by " + GOOGLE_COM);
				   
			   }
			   
		   }
		   else{
			   System.out.println("Largest search results value is " + bingResult +" showing by " + BING_COM);
			   if(googleResult>yahooResult){
				   System.out.println("Medium search results value is " + googleResult +" showing by " + GOOGLE_COM);
				   System.out.println("Smallest search results value is " + yahooResult +" showing by " +  YAHOO_COM);}
			   else{
				   System.out.println("Medium search results value is " + yahooResult +" showing by " +  YAHOO_COM);
				    System.out.println("Smallest search results value is " + googleResult +" showing by " + GOOGLE_COM);
			   }
			   }
			   
		   };
		   /*int[] resultArray={googleResult, yahooResult, bingResult};  
		   Arrays.sort(resultArray);  
		   System.out.println("Minimum = " + resultArray[0]); 
		   System.out.println("Medium " + resultArray[(resultArray.length-1)/2]);
		   System.out.println("Maximum = " + resultArray[resultArray.length-1]);  */
		   
	   }


