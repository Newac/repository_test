package tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import integration.IntegrationBase;

import pagemodel.SearchPage;

public class CompareResultNumber extends IntegrationBase{

	public CompareResultNumber(String browser) {
		super(browser);
		
	}
	
	  private SearchPage searchPage;
	  
	    @BeforeMethod
	    public void setupHomePage() {
	        searchPage = new SearchPage(driver);
	    }
	    
	    @Test
	  public void firstFormTest() {	     
		/*searchPage.openGooglepage("saucelab");
		searchPage.waitForPageToLoad();
	      int googleResult = searchPage.countWords();
	      
	  	searchPage.openYahoopage("saucelab");
		searchPage.waitForPageToLoad();
	      int yahooResult = searchPage.countWords();
	      
	  	searchPage.openBingpage("saucelab");
		searchPage.waitForPageToLoad();
	      int bingResult = searchPage.countWords();*/
	    	searchPage.compareResultNumber("saucelab");
	    	searchPage.compareResultNumber("github");
	    	searchPage.compareResultNumber("bitbucket");
	    }
	    
	    
	

}
